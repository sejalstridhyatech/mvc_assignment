<div class="album py-5 bg-light">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="card border-success" style="max-width: 65rem;padding: 2%;">
                    <h2> Registration </h2> <hr>
                    <div class="card-body">
                        <form method="post" enctype="multipart/form-data">
                            <div class="row mb-3">
                                <div class="col">
                                    <label for="fname" class="form-label">First Name</label>
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required="">
                                </div>
                                <div class="col">
                                    <label for="lname" class="form-label">Last Name</label>
                                    <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" required="">
                                </div>
                                <div class="col">
                                    <label for="mobile" class="form-label">Contact Number</label>
                                    <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="1234567890" required="">
                                </div>
                                
                            </div>
                            <div class="row mb-3">
                            <div class="col">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required="">
                                </div>
                                <div class="col">
                                    <label for="password" class="form-label"> Confirm Password</label>
                                    <input type="password" class="form-control" id="cpassword" name="password" placeholder=" confirm password" required="">
                                </div>
                            </div>
                            <div class="row mb-3">
                            <div class="col">
                                    <label for="gender" class="form-label">Gender</label><br>
                                    <input type="radio" id="gender" name="gender" value="Male" checked>Male
                                    <input type="radio" id="gender" name="gender" value="Female">Female
                                </div>
                                <div class="col">
                                    <label for="inputdesignation" class="form-label">Designation</label>
                                    <select class="form-select" id="inputdesignation" name="designation" aria-label="Default select example" required="">
                                        <option selected disabled>Select</option>
                                        <option>HR</option>
                                        <option>PHP Developer</option>
                                        <option>Android Developer</option>
                                        <option>Senior Developer</option>
                                        <option>Junior Developer</option>
                                        <option>project Manager</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <label for="profile" class="form-label">Profile</label><br>
                                    <input type="file" class="form-control-file" name="profile" id="profile">
                                </div>
                                <div class="col">
                                    <label for="hobbies" class="form-label">Hobbies</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="hobbies[]" value="Travelling">
                                        <label class="form-check-label" for="inlineCheckbox1">Travelling</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="hobbies[]" value="Music">
                                        <label class="form-check-label" for="inlineCheckbox2">Music</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="hobbies[]" value="Coding">
                                        <label class="form-check-label" for="inlineCheckbox3">Coding</label>
                                    </div>
                                </div>

                            </div><br>
                            <div class="row mb-3">
                            <div class="col">
                                <input type="submit" name="regist" id="regist" value="Registration" class="btn btn-success"></div>
                                <div class="col">
                                Already Have An Account ! <a  class="btn btn-primary" href="login">Login</a>
                                <?php
                    // if($_SESSION['user_data']){
                    //         echo '<a  class="btn btn-success" href="logout">Logout</a>';
                    //     } else {
                    //         if($_SERVER['PATH_INFO'] == '/login'){
                    //             echo '<a  class="btn btn-success" href="register">Register</a>';
                    //         } else if($_SERVER['PATH_INFO'] == '/register') {
                    //             echo '<a  class="btn btn-success" href="login">Login</a>';
                    //         }
                    //     }

                    ?>
                               <!-- Already Have An Account ! <a  class="btn btn-primary" href="login">Login</a>-->
                                </div>
                            </div>
                        </form>
                        <!-- <script>
  $("#form").submit(function(){
     if($("#password").val()!=$("#cpassword").val())
     {
         alert("password should be same");
         return false;
     }
 })
</script> -->
                    </div>
                </div>
            </div>
        </div>