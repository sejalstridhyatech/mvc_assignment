<?php error_reporting(0); ?>
<div class="album py-5 bg-light" style="height:180vh;">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="card border-success" style="max-width: 65rem;padding: 2%;">
            <h2> Profile </h2>
            <hr>
            <div class="card-body">
                <form method="post" id="profile">
                    <img src="<?php echo 'uploads/' . $selectData['Data'][0]->profile;  ?>" class="rounded mx-auto d-block" height="200" width="200" alt="Profile Photo">
                    <div class="row mb-3">
                        <div class="col">
                            <label for="fname" class="form-label">First Name</label>
                            <input type="text" class="form-control" id="fname" name="fname" placeholder="<?php echo $selectData['Data'][0]->fname;  ?>" value="<?php echo $selectData['Data'][0]->fname; ?>" required="">
                        </div>
                        <div class="col">
                            <label for="lname" class="form-label">Last Name</label>
                            <input type="text" class="form-control" id="lname" name="lname" placeholder="<?php echo $selectData['Data'][0]->lname;  ?>" value="<?php echo $selectData['Data'][0]->lname; ?>" required="">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $selectData['Data'][0]->email;  ?>" value="<?php echo $selectData['Data'][0]->email; ?>" required="">
                        </div>
                        <div class="col">
                            <label for="mobile" class="form-label">Contact Number</label>
                            <input type="tel" class="form-control" id="mobile" name="mobile" placeholder="<?php echo $selectData['Data'][0]->contact;  ?>" value="<?php echo $selectData['Data'][0]->contact; ?>" required="">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col">
                            <label for="password" class="form-label"> Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="<?php echo $selectData['Data'][0]->password;  ?>" value="<?php echo $selectData['Data'][0]->password; ?> ">
                        </div>
                        <div class="col">
                            <label for="password" class="form-label"> Confirm Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="<?php echo $selectData['Data'][0]->password;  ?>" value="<?php echo $selectData['Data'][0]->password; ?> ">
                        </div>
                    </div>
                    <div class="row mb-3">

                        <div class="col">
                            <label for="gender" class="form-label">Gender</label><br>
                            <input type="radio" id="gender" name="gender" value="Male" <?php
                                                                                        if ($selectData['Data'][0]->gender == 'Male') {
                                                                                            echo "checked";
                                                                                        }
                                                                                        ?>>Male
                            <input type="radio" id="gender" name="gender" value="Female" <?php
                                                                                            if ($selectData['Data'][0]->gender == 'Female') {
                                                                                                echo "checked";
                                                                                            }
                                                                                            ?>>Female
                        </div>

                        <div class="col">
                            <label for="inputdesignation" class="form-label">Designation</label>
                            <select class="form-select" id="inputdesignation" aria-label="Default select example" required="">
                                <option disabled>Select</option>
                                <option <?php
                                        if ($selectData['Data'][0]->designation == 'HR') {
                                            echo "selected";
                                        }
                                        ?>>HR</option>
                                <option <?php
                                        if ($selectData['Data'][0]->designation == 'PHP Developer') {
                                            echo "selected";
                                        }
                                        ?>>PHP Developer</option>
                                <option <?php
                                        if ($selectData['Data'][0]->designation == 'Android Developer') {
                                            echo "selected";
                                        }
                                        ?>>Android Developer</option>
                                <option <?php
                                        if ($selectData['Data'][0]->designation == 'Senior Developer') {
                                            echo "selected";
                                        }
                                        ?>>Senior Developer</option>
                                <option <?php
                                        if ($selectData['Data'][0]->designation == 'Junior Developer ') {
                                            echo "selected";
                                        }
                                        ?>>Junior Developer</option>
                                <option <?php
                                        if ($selectData['Data'][0]->designation == 'Project Manager') {
                                            echo "selected";
                                        }
                                        ?>>Project Manager</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col">
                            <?php $hobbies_arr = explode(',', $selectData['Data'][0]->hobbies); ?>
                            <label for="hobbies" class="form-label">Hobbies</label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" name="hobbies[]" value="Travelling" <?php if (in_array('Travelling', $hobbies_arr)) {
                                                                                                                                                echo 'checked';
                                                                                                                                            } ?>>
                                <label class="form-check-label" for="inlineCheckbox1">Travelling</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox2" name="hobbies[]" value="Music" <?php if (in_array('Music', $hobbies_arr)) {
                                                                                                                                        echo 'checked';
                                                                                                                                    } ?>>
                                <label class="form-check-label" for="inlineCheckbox2">Music</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="inlineCheckbox3" name="hobbies[]" value="Coding" <?php if (in_array('Coding', $hobbies_arr)) {
                                                                                                                                            echo 'checked';
                                                                                                                                        } ?>>
                                <label class="form-check-label" for="inlineCheckbox3">Coding</label>
                            </div>
                        </div><br><br><br>
                        <div class="row mb-6">
                            <div class="col">
                                <input type="submit" name="updateUser" id="updateUser" value="Update" class="btn btn-primary <?php echo $user->id; ?>">
                            </div>
                            <div class="col">
                                <a class="btn btn-success" href="logout">Logout</a>
                            </div>
                        </div>

                </form>
            </div>
        </div>
    </div>
</div>