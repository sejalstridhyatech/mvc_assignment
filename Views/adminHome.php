<br>
<div class="container col-md-6">
    <div class="row mb-3">
        <div class="col">
            <a class="btn btn-success" style="margin-right:100px; margin-left:100px;" href="logout">Logout</a>
        </div>
        <div class="col">
            <a class="btn btn-primary" style="margin-left:100px; margin-right:50px;" href="register">+ Add User</a>
        </div>
    </div>
</div>

<h5 class="card-header" style="text-align:center;">Employee List</h5>

<div class="album py-5 bg-light" style="height:100vh;">
    <div class="row h-100 justify-content-center">

        <table class="table table-hover" style="max-width: 80rem;">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Fname</th>
                    <th scope="col">Lname</th>
                    <th scope="col">Email</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Gender</th>
                    <th scope="col">Designation</th>
                    <th scope="col">hobbies</th>
                    <th scope="col">Profile</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>

                <?php
                $i = 1;
                foreach ($users as $user) {
                ?>

                    <tr>
                        <th scope="row"><?php echo $i; ?></th>
                        <td><?php echo $user->fname; ?></td>
                        <td><?php echo $user->lname; ?></td>
                        <td><?php echo $user->email; ?></td>
                        <td><?php echo $user->contact; ?></td>
                        <td><?php echo $user->gender; ?></td>
                        <td><?php echo $user->designation; ?></td>
                        <td><?php echo $user->hobbies; ?></td>
                        <td>
                            <img src="<?php echo 'uploads/' . $user->profile; ?>" alt="alt" height="80px" width="80px" />
                        </td>
                        <td>
                            <a href="update?user=<?php echo $user->id; ?>" class="btn btn-warning">Edit</a>
                            <a href="adminHome?user=<?php echo $user->id; ?>" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                <?php
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>

</div>