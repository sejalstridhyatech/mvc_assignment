-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 10, 2023 at 04:41 AM
-- Server version: 8.0.31
-- PHP Version: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` tinyint NOT NULL DEFAULT '0' COMMENT '0:user,1:admin',
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `contact` bigint NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `designation` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `profile` text NOT NULL,
  `hobbies` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `users'
--

INSERT INTO `users` (`id`, `role_id`, `fname`, `lname`, `email`, `pass`, `contact`, `gender`, `designation`, `profile`, `hobbies`) VALUES
(6, 1, 'Sejal', 'Sanodiya', 'sanodiyasejal2002@gmail.com', '$2y$10$vWuCYuvIPMx4yxAev4gfNeZYeBRSvCWPxYZsdQojch3xevRD5Bme6', 9175755300, 'Female', 'PHP Developer', 'Sejal_20230209171450.jpg', 'Music,Coding'),
(2, 1, 'JANVI', 'PATEL', 'janvi.patel@gmail.com', '$2y$10$B7Sgm.njGTsIoTmImbtsaeraWuoJfXkTTvSb73p7NQNUazbEl6FFW', 8879657865, 'Female', 'PHP Developer', 'JANVI_20230209125215.jpg', 'Travelling,Music,Coding'),
(3, 0, 'Manshi', 'Sanodiya', 'manshisano@gmail.com', '$2y$10$5EtaXUL7eI/ygqEUtynPS.TizgsNh8JSQHFLPFkvIMfmCt/uAgDje', 9191223234, 'Female', 'Junior Developer', 'Manshi_20230209131720.jpg', 'Travelling'),
(4, 0, 'Sai', 'Sanodiya', 'saisano@gmail.com', '$2y$10$hCGr89sI.qGePtP6dVBEvuMDXk.XzTvI2n1ve0a8J5PyLcRc8t8nq', 8888899999, 'Male', 'Junior Developer', 'Sai_20230209134004.jpg', 'Travelling,Music'),
(7, 0, 'Devanshi', 'Parmar', 'devanshi@gmail.com', '$2y$10$MVWWn1q5mofAZy8F.AV1aeBIx9UYbc8n4vjpGfqSmDb6bfooYUCje', 8970657809, 'Female', 'Junior Developer', 'Devanshi_20230209180317.jpg', 'Music'),
(8, 0, 'Mohit', 'Gupta', 'mohitgupta12@gmail.com', '$2y$10$4wpfWkv/L8SDz5Zvh7B4OOumeTdvJH4vix80859bl6y55IOiXuG6K', 9878903211, 'Male', 'PHP Developer', 'Mohit_20230209181925.jpg', 'Music,Coding');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
